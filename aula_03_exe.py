# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:

# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'

# def sauda(nome):
#     print(f"Ola {nome}!, Tudo bem com voce?")

# sauda(nome = input("Por favor digite seu nome: "))

# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

# num1 = int(input("Digite o primeiro numero: "))
# num2 = int(input("Digite o segundo numero: "))
# print("""
#       Opcoes de operacao:
#       # 1 - soma
#       # 2 - subtracao
#       # 3 - multiplicacao
#       # 4 - divisao

#       """)
# oper = int(input("Digite aqui a operacao escolhida: "))
# def soma(num1,num2):
#     resultado = num1 + num2
#     print(f"O resultado da soma entre {num1} e {num2} eh {resultado}")

# def subt(num1,num2):
#     resultado = num1 - num2
#     print(f"O resultado da subtracao de {num1} e {num2} eh {resultado}")

# def mult(num1,num2):
#     resultado = num1 * num2
#     print(f"O resultado da multiplicacao entre {num1} e {num2} eh {resultado}")

# def divi(num1,num2):
#     resultado = num1 / num2
#     print(f"O resultado da divisao entre {num1} e {num2} eh {resultado}")

# if oper == 1:
#     soma(num1,num2)
# elif oper == 2:
#     subt(num1,num2)
# elif oper == 3:
#     mult(num1,num2)
# elif oper == 4:
#     divi(num1,num2)
# else:
#     print("Voce escolheu uma opcao invalida")


# ====================================================
# Exercicio 3:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações 
# em funções.
# import os
# cesta = []
# valor_gasto = 0

# def opt1(cesta):
#     if len(cesta) < 1:
#         if os.name == "nt":
#             os.system("cls")
#         else:
#             os.system("clear")
#         print("A cesta está vazia")
#     else:
#         if os.name == "nt":
#             os.system("cls")
#         else:
#             os.system("clear")
#         print(cesta)

# def opt2(opcao_fruta,cesta,valor_gasto):
#     while True:
#         print("""
#         Menu de frutas:
#         Escolha fruta desejada:
#         1 - Banana   R$2.50
#         2 - Melancia R$8.00
#         3 - Morango  R$6.00
            
#         """)

#         opcao_fruta = input("Selecione a fruta para adicionar na cesta. ")

#         if opcao_fruta == "1":
#             cesta.append("Banana")
#             valor_gasto = valor_gasto + 2.50
#             if os.name == "nt":
#                 os.system("cls")
#             else:
#                 os.system("clear")
#             print("'Banana' foi adicionada a cesta.")
#             break
#         elif opcao_fruta == "2":
#             cesta.append("Melancia")
#             valor_gasto = valor_gasto + 8.00
#             if os.name == "nt":
#                 os.system("cls")
#             else:
#                 os.system("clear")
#             print("'Melancia' foi adicionada a cesta.")
#             break
#         elif opcao_fruta == "3":
#             cesta.append("Morango")
#             valor_gasto = valor_gasto + 6.00
#             if os.name == "nt":
#                 os.system("cls")
#             else:
#                 os.system("clear")
#             print("'Morango' foi adicionada a cesta.")
#             break
#         else:
#             if os.name == "nt":
#                 os.system("cls")
#             else:
#                 os.system("clear")
#             print("Valor inválido, digite novamente.")

# def opt3(valor_gasto):
#         print(f"""Conteúdo da cesta: {cesta}
#                 Valor gasto: R${valor_gasto:.2f}
#             """)

# while True:
#     print("""
#         Quitanda:
#         1: Ver cesta
#         2: Adicionar frutas
#         3: Sair

#         """)

#     opcao = input("Selecione uma das opções acima. ")

#     if opcao == "1":
#         opt1(cesta)
#     elif opcao == "2":
#         opt2(opcao_fruta,cesta,valor_gasto)            
#     elif opcao == "3":
#         opt3(valor_gasto)
#     else:
#         print("Valor inválido, digite novamente.")

# ====================================================
# Exercicio 4:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

# count = 0
# lista = []
# paircount = 0
# divresult = 0
# def logica(count,divresult,lista,paircount):
#     for count in range(0, (len(lista))):
#         divresult = int(lista[count]) % 2

# while True:
#     num = input("Digite um numero de 1 a 9 ou # para terminar ")
#     if num == "#":
#         break
#     lista.append(num)
#     print(f"Voce digitou o numero {num}.")
#     logica(count,divresult,lista,paircount)
#     if divresult == 0:
#         paircount=paircount + 1
#     if paircount == 1:
#         print(f"Voce digitou {paircount} vez numeros pares")
#     elif paircount > 1:
#         print(f"Voce digitou {paircount} vezes numeros pares")
 

# ====================================================
# Exercicio 5:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

altura = float(input("Digite a altura da parede: "))
largura = float(input("Digite a Largura da parede: "))
area = altura * largura
tinta = area / 3
print(f"Voce precisa de {tinta} litros de tinta para pintar sua parede")
