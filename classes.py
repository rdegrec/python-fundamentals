# # Manipulação de arquivos
# Class / Classes
# Crie uma classe pra simular uma pilha, onde itens só podem ser removidos do topo.

# # r -> READ   -> Leitura
# # w -> WRITE  -> Escrita (SOBRESCREVE)
# # a -> APPEND -> Anexar
# # + -> Leitura e escrita
class Pilha:

# arquivo = open("texto.txt", "a")
    def __init__(self):
        self.pilha = []
        self.__topo = 0

# conteudo = "\nQuarta linha do arquivo de texto"
    def empilhar(self, item):
        self.pilha.append(item)
        self.__topo += 1

# arquivo.write(conteudo)
    def desempilhar(self):
        if self.__topo > 0:
            ultimo_item = self.pilha[-1]
            self.pilha.remove(ultimo_item)
            self.__topo -= 1
            return "Item removido"
        else:
            return "Nenhum item empilhado"
        
    def checar(self):
        return self.pilha
        
balcao = Pilha()
print(balcao.pilha)

# arquivo.close()
balcao.empilhar("Prato de vidro verde")
balcao.empilhar("Prato de porcelana azul")
balcao.empilhar("Prato de metal")
balcao.empilhar("Prato de madeira")

# #====================================================
# arquivo = open("texto.txt", "r")
balcao.desempilhar()
balcao.desempilhar()

# conteudo = arquivo.readlines()
# for linha in conteudo:
#     print(linha, end="")
Encapsulamento

# arquivo.close()
print(balcao.checar())

# Herança

# import csv
class Funcionario:

# with open("registro.csv", "r") as arquivo:
#     conteudo = csv.reader(arquivo, delimiter=";")
#     # cabeçalho = next(conteudo)
#     # primeira_linha = next(conteudo)
#     # segunda_linha = next(conteudo)
#     # terceira_linha = next(conteudo)
    def __init__(self):
        self.__nome = ""
        self.__idade = 0
        self.__salario = 0

#     lista_conteudo = []

#     for linha in conteudo:
#         lista_conteudo.append(linha)

#     for linha in lista_conteudo:
#         print(linha)
class Gerentes(Funcionario):

# with open("registro.csv", "a+", newline="") as arquivo:
#     escrita = csv.writer(arquivo, delimiter=";")
    def __init__(self):
        super().init__()
        self.__bonus = "25%"

#     escrita.writerow(["444444444", "Tiago P", 27, "M", "Brasileiro"])

# CASE / MATCH -> 3.10
    
# nome = input("Digite um nome: ")

# match nome:

#     case "Tiago":
#         print("Caramba, que belo nome!")

#     case "Caio":
#         print("Que nome estranho.")

#     case "João":
#         print("Que nome diferente!")
# Polimorfismo
class Cliente:

#     case _:
#         print("Não conheço esse nome.")
    def __init__(self):
        self.carrinho = []
        self.cpf = ''
        self.total = 0

    def adicionar_item(self, item):
        self.carrinho.append(item)

# ESCOPO - SCOPE

x = 300
# Variavel Global - Escopo global

def funcao():
    # global x
    x = 250
    # Variavel Local - Escopo Local

    def caixa(self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total
    
class ClienteVIP(Cliente):

funcao()
print(x)
    def __init__(self):
        super().__init__()
        self.desconto = 0.95

#                                 COFFEE BREAK 16:02 ~ 16:17
    def caixa(self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total * self.desconto
