# 1) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

contagem = 0
with open("faroeste.txt", "r", encoding="utf-8") as arquivo:
    conteudo = arquivo.read()
    vogais = 'aeiouAEIOUáéíóúÁÉÍÓÚâêôÂÊÔãõÃÕ'
    for letra in conteudo:
        if letra in vogais:
            contagem = contagem + 1
    print(contagem)

#=============================
    
# def contar_vogais(texto):
#     vogais = 'aeiouAEIOUáéíóúÁÉÍÓÚâêôÂÊÔãõÃÕ'
#     contagem = 0

#     for letra in texto:
#         if letra in vogais:
#             contagem += 1

#     return contagem

# def main():
#     try:
#         with open('faroeste_caboclo.txt', 'r', encoding='utf-8') as arquivo:
#             letra_musica = arquivo.read()

#         total_vogais = contar_vogais(letra_musica)
#         print(f"Total de vogais na música Faroes Caboclo: {total_vogais}")

#     except FileNotFoundError:
#         print("Arquivo não encontrado. Por favor, verifique o nome do arquivo e tente novmente.")

# if __name__ == "__main__":
#     main()


# ====================================================================================
# 2) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;



# ====================================================================================
# 3) Implemente uma função de consulta no programa do exercício 2.


# ====================================================================================
# 4) Implemente uma função de exclusão no programa do exercício 2.
