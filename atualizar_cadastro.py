import os
import csv

def consulta():
    resposta = int(input("""Qual valor deseja consultar?
0 - CPF
1 - Nome 
"""))

    valor = input("Digite o CPF/Nome a consultar: ")

    with open("banco.csv", "r") as arquivo:
        conteudo = csv.reader(arquivo, delimiter=";")
        for linha in conteudo:
            if linha[resposta] == valor:
                print(f"""Cadastro encontrado:
CPF: {linha[0]}
NOME: {linha[1]}
IDADE: {linha[2]}
SEXO: {linha[3]}
ENDEREÇO: {linha[4]}""")

def exculsao(dado_a_excluir):
    with open("banco.csv", "r") as arquivo_inicial, open("banco_editado.csv", "a") as arquivo_final:
        writer = csv.writer(arquivo_final, delimiter=";")

        for linha in csv.reader(arquivo_inicial, delimiter=";"):
            if linha[0] != dado_a_excluir and linha[1] != dado_a_excluir:
                writer.writerow(linha)
        
    os.remove("banco.csv")
    os.rename("banco_editado.csv", "banco.csv")

while True:
    opcao = input("""Digite uma opção:
1 - Consultar um valor no banco.
2 - Cadastrar um valor no banco.
3 - Remover um valor do banco.
4 - Parar o programa.""")

    if opcao == "4":
        print("Parando a execução")
        break
    
    elif opcao == "3":
        entrada = input("Digite o nome ou CPF da pessoa que deseja demover do banco: ")
        exculsao(entrada)
        print(f"Entrada {entrada} removida do banco de dados.")

    elif opcao == "2":
        cpf = input("Qual o CPF da pessoa? ")
        nome = input("Qual o nome da pessoa? ")
        idade = input("Qual o idade da pessoa? ")
        sexo = input("Qual o sexo da pessoa? ")
        endereco = input("Qual o endereço da pessoa? ")

        pessoa = (cpf, nome, idade, sexo, endereco)

        with open("banco.csv", "a+", newline="") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(pessoa)

        print(f"'{nome}' cadastrada no banco com sucesso.")

    elif opcao == "1":
        consulta()

    else:
        print("O valor digitado não é válido.")
