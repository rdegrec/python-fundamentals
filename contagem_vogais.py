def isVowel(letra):
    if letra.lower() in "aeiouáéíóúãẽĩõũâêîôûàèìòùäëïöü":
        return True
    else:
        return False

vogais = 0
consoantes = set()

# conteudo = ["asdasdas", "asdasd", "asdasd"]
# #                0          1         2

# linha    = "asdasdasda"
# #           0123456789

with open("faroeste.txt", "r", encoding="UTF-8") as arquivo:
    conteudo = arquivo.readlines()
    for linha in conteudo:
        for letra in linha:
            if isVowel(letra) == True:
                vogais += 1
            else:
                consoantes.add(letra)

print(f"Vogais: {vogais}")
print(f"Consoantes:\n {consoantes}")
