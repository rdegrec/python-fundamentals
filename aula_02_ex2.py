idade=int(input("Qual a sua idade?"))
# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual
ano=2024-idade
if ano <= 1964:
    print("Baby boomer")
elif ano >= 1965 and ano <= 1979:
    print("Geracao X")
elif ano >= 1980 and ano <= 1994:
    print("Geracao Y")
elif ano >= 1995 and ano <= 2024:
    print("Geracao Z")
