# IF - Condição

idade = int(input("Qual é a sua idade? "))

if idade ==+ 18:
    print("Voce é maior de idade.")

elif idade > 15:
    print("Voce tem pelo menos 15 anos.")

elif idade > 10:
    print("Voce tem pelo menos 10 anos.")

else:
    print("Voce tem menos de 10 anos.")


# FOR / WHILE - Repetição
    
for numero in range(0, 10):
    print(numero)


resposta = "nao"
while resposta != "sim":
    print("Imprimindo mensagem do loop")
    resposta = input("Quer parar o loop? [sim/nao] ")


while True:
    resposta = input("Quer parar o loop? [sim/nao] ")
    if resposta == "sim":
        break
