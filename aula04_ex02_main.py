# =============================================================
# 2) Crie um programa utilizando dois arquivos, onde um deles possui todas as funcçoes utilizadas na aplicação.
# Onde o programa deverá perguntar ao usuario nome/idade de uma pessoa, e armazenar esses valores em um dicionario,
# e repetir essa ação até que a pessoa não queira mais adicionar nomes, em seguida, o programa deverá mostrar o numero
# de pessoas em categorias de acordo com a idade:
# 0-17 anos: Menor de idade
# 18-59 anos: Adulto
# 60+ anos: Idoso
# E deverá perguntar para o usuario se ele gostaria de exibir na tela uma lista com os nomes das pessoas de cada grupo,
# ou se o usuario deseja finalizar o programa.

def getdata():
    adultos = 0
    menores = 0
    idosos = 0
    count = 0
    grupom = []
    grupoi = []
    grupoa = []
    listanomes = []
    dados = {}
    while True:
        nome = input("Digite o nome a ser inserido ")
        listanomes.append(nome)
        idade = input(f"Digite a idade de {nome} ")
        dados[nome] = idade
        opt = input("Deseja continuar a inserir novos nomes? (S/N): ")
        if opt == "N":
                break
    quant = len(listanomes)
    while quant > 0:
        print(f" contador {count}")
        print(listanomes)
        nomes = listanomes[count]
        age = int(dados[nomes])
        print(f" lista nomes {listanomes}")
        print(f" dicionario {dados}")
        print(f" nome atual {nomes}")
        print(f" idade atual {age}")
        if age <= 17:
            menores = menores + 1
            grupom.append(nomes)
            count = count + 1
        elif age <= 59:
            adultos = adultos + 1
            grupoa.append(nomes)
            count = count + 1
        elif age >= 60:
            idosos = idosos + 1
            grupoi.append(nomes)
            count = count + 1
        quant = quant -1

    print(f"O numero de menores eh {menores}.")
    print(f"O numero de adultos eh {adultos}.")
    print(f"O numero de idosos eh {idosos}.")

    opt2 = input("Deseja exibir a lista de cada uma das categorias? S/N: ")
    if opt2 == "N":
        return
    elif opt2 == "S":
        print(f"""
            Menores: {grupom}.
            Adultos: {grupoa}.
            Idosos: {grupoi}.
        """)

getdata()
