# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o pacote random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".

# import random
# from time import sleep
# participantes = ["Aline","Eva","Alice","Tutu","Rita","Bruna","Gentil","Liliam","Odila","Rodrigo"]
# cadeiras = 10
# while True:
#     aleatorio=random.randint(0, len(participantes) -1)
#     repetidos = []
#     if aleatorio not in repetidos:
#         cadeiras = cadeiras - 1
#         repetidos.append(aleatorio)
#         escolhido=participantes.pop(aleatorio)
#         print(f"{escolhido} foi eliminado(a)")
#         sleep(3)
#         if cadeiras == 1:
#             print(f"O vencedor foi {participantes}")
#             break


# =============================================================
# 2) Crie um programa utilizando dois arquivos, onde um deles possui todas as funcçoes utilizadas na aplicação.
# Onde o programa deverá perguntar ao usuario nome/idade de uma pessoa, e armazenar esses valores em um dicionario,
# e repetir essa ação até que a pessoa não queira mais adicionar nomes, em seguida, o programa deverá mostrar o numero
# de pessoas em categorias de acordo com a idade:
# 0-17 anos: Menor de idade
# 18-59 anos: Adulto
# 60+ anos: Idoso
# E deverá perguntar para o usuario se ele gostaria de exibir na tela uma lista com os nomes das pessoas de cada grupo,
# ou se o usuario deseja finalizar o programa.

# from aula04_ex02_main import getdata

# getdata()

# from funcoes import definirCategoria
# dicionario = {}

# while True:
#     resposta = int(input("""Voce deseja adicionar um item ao dicionário?
# 1 - Adicionar um item.
# 2 - Finalizar o dicionario.
# """))

#     if resposta == 2:
#         break

#     nome = input("Qual o nome da pessoa a ser adicionada? ")
#     idade = int(input("Qual a idade dessa pessoa? "))

#     dicionario[nome] = idade

# definirCategoria(dicionario)


# =============================================================
# 3) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

# lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
# "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
# "Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

numero_part = int(input("Quantas pessoas vão participar do sorteio? [2~20]: "))
n_sorteios = int(input("Quantas pessoas serão sorteadas? "))

participantes = []

for loop in range(0, numero_part):
    escolhido = choice(lista)
    participantes.append(escolhido)
    lista.remove(escolhido)

print(f"Os participantes serão: {participantes}")

for sorteio in range(0, n_sorteios):
    ganhador = choice(participantes)
    print(f"{ganhador} venceu o sorteio!")
    participantes.remove(ganhador)
