# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# placa
# modelo
# movimento

# Os comportamentos esperados para um Ônibus são:

# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.


class Onibus:
    def __init__(self):
        self.passageiros = []
        self.atual = 0
        self.lugares = 45
        self.placa = ''
        self.modelo = ""
        self.status = 0


    def motorista(self):
        print("O onibus saiu da garagem. Opcoes do motorista: ")
        self.status = 1
        while True:
            print("""
                        1 - Embarcar
                        2 - Desembarcar
                        3 - Acelerar
                        4 - Frear
            """)
            opcao = input("Digite a opcao desejada: ")
            if opcao == 1 and self.status == 1:
                print("Voce precisa frear o onibus antes de embarcar passageiros!")
            elif opcao == 1 and self.status == 0:
                    if self.lugares == 45:
                        print("O onibus esta lotado!")
                    elif self.lugares < 45:
                        passageiro = input("Digite o nome do passageiro: ")
                        self.passageiros.append(passageiro)
                        self.lugares = self.lugares - 1
                        self.atual = self.atual + 1
            if opcao == 3 and self.status == 1:
                print("Velocidade maxima atingida. Vai devagar, moto!")
            elif opcao == 3 and self.status == 0:
                print("Onibus em movimento! Voce nao pode embarcar/desembarcar")
            if opcao == 4 and self.status == 0:
                print("Onibus ja esta parado.")
            elif opcao == 4 and self.status == 1:
                self.status = 0
                print("Onibus parado. Voce pode embarcar/desembarcar passageiros")
            if opcao == 2 and self.status == 1:
                print("Onibus em movimento! Voce nao pode embarcar/desembarcar")
            if opcao == 2 and self.status == 0:
                if self.lugares == 0:
                    print("Onibus vazio. Nao ha ninguem para desembarcar.")
                elif self.lugares > 1:
                    for pessoa in self.passageiros:
                        pessoa = pessoa + 1
                        print(f"""Lista de passageiros:
                            {pessoa} - {self.passageiros[pessoa]}""")
                desembarque = input("Digite o numero referente a pessoa a desembarcar: ")
                print(f"{self.passageiros[desembarque]} desceu do onibus.")
                self.passageiros.pop(desembarque)

busao = Onibus()
busao.motorista()



# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila
