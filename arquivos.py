# Manipulação de arquivos

# r -> READ   -> Leitura
# w -> WRITE  -> Escrita (SOBRESCREVE)
# a -> APPEND -> Anexar
# + -> Leitura e escrita

arquivo = open("texto.txt", "a")

conteudo = "\nQuarta linha do arquivo de texto"

arquivo.write(conteudo)

arquivo.close()

#====================================================
arquivo = open("texto.txt", "r")

conteudo = arquivo.readlines()
for linha in conteudo:
    print(linha, end="")

arquivo.close()


import csv

with open("registro.csv", "r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")
    # cabeçalho = next(conteudo)
    # primeira_linha = next(conteudo)
    # segunda_linha = next(conteudo)
    # terceira_linha = next(conteudo)

    lista_conteudo = []

    for linha in conteudo:
        lista_conteudo.append(linha)

    for linha in lista_conteudo:
        print(linha)

with open("registro.csv", "a+", newline="") as arquivo:
    escrita = csv.writer(arquivo, delimiter=";")

    escrita.writerow(["444444444", "Tiago P", 27, "M", "Brasileiro"])

